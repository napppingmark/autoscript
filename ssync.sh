#!/usr/bin/bash

echo "ssync.sh is an automated script for the installation of simplified applications."

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
#Download the package from the server. FTP is required.
PACKAGE_DOWNLOAD(){
	clear
	echo -e "\e[1;33mCredits to the one who wrote the PACKAGE_DOWNLOAD script\e[0m \n"
	echo -e "Enter CRQ Number: \c"
	read CRQ_NUM
	#echo -e "File to Move: \c"
	#read FMO
	#echo -e "File to Move: \c"
	#read FMV
	echo -e "Enter the Installation Package's URL: \c"
	read PKG_URL
	URL_PATH=/`echo $PKG_URL | cut -d"/" -f4-5`                            #/noProduccion/XX
	ZIP=`echo $PKG_URL | cut -d"/" -f 6`                                   #<Application_Name>InstalablesSH_ES_<Date>_<Version>_<Tag>.zip
	IPN=`echo $PKG_URL | cut -d"/" -f 6 | cut -f1 -d "."`                  #Installation Package Name
	AN_TEMP=`echo $ZIP | cut -d"_ES" -f 1 | sed 's/InstalablesSH/''/'`     #Application Name
	Abbrev=`echo $PKG_URL | cut -d"/" -f 5`
	if [[ ${AN_TEMP} == DR ]]
	then
		AN=`echo $ZIP | cut -d"_ES" -f 1-5 | sed 's/InstalablesSH/''/'`
		Da=`echo $ZIP | cut -d"_ES" -f 7`
		VN=`echo $ZIP | cut -d"_ES" -f 8-12`
		T=`echo $ZIP | cut -d"_ES" -f 13-14 | cut -f1 -d "."`
	elif [[ ${AN_TEMP} == DWH ]] || [[ ${AN_TEMP} == SF ]]
	then
		AN2=`echo $ZIP | cut -d"_ES" -f 1-3 | sed 's/InstalablesSH/''/'`
		if [[ ${AN2} == DWH_Comercial_TiendaLogistica ]]
		then
			AN=DW_DWH_Comercial_Aprov_desde_TiendaLogistica
			Da=`echo $ZIP | cut -d"_ES" -f 5`
			VN=`echo $ZIP | cut -d"_ES" -f 6-10`
			T=`echo $ZIP | cut -d"_ES" -f 11-12 | cut -f1 -d "."`
		else
			AN=${AN2}
			Da=`echo $ZIP | cut -d"_ES" -f 5`
			VN=`echo $ZIP | cut -d"_ES" -f 6-10`
			T=`echo $ZIP | cut -d"_ES" -f 11-12 | cut -f1 -d "."`
		fi
	elif [[ ${AN_TEMP} == DW ]]
	then
		AN=`echo $ZIP | cut -d"_ES" -f 1-6 | sed 's/InstalablesSH/''/'`
		Da=`echo $ZIP | cut -d"_ES" -f 8`
		VN=`echo $ZIP | cut -d"_ES" -f 9-13`
		T=`echo $ZIP | cut -d"_ES" -f 14-15 | cut -f1 -d "."`
	elif [[ ${AN_TEMP} == DY ]]
	then
		AN=`echo $ZIP | cut -d"_ES" -f 1-2`
		Da=`echo $ZIP | cut -d"_ES" -f 5`
		VN=`echo $ZIP | cut -d"_ES" -f 6-10`
		T=`echo $ZIP | cut -d"_ES" -f 11-12 | cut -f1 -d "."`
	elif [[ ${AN_TEMP} == DWHClientes ]] || [[ ${AN_TEMP} == EX ]] || [[ ${AN_TEMP} == GestionDatos ]] || [[ ${AN_TEMP} == ExtraccionesDWH ]]
	then
		AN=`echo $ZIP | cut -d"_ES" -f 1-2 | sed 's/InstalablesSH/''/'`
		Da=`echo $ZIP | cut -d"_ES" -f 4`
		VN=`echo $ZIP | cut -d"_ES" -f 5-9`
		T=`echo $ZIP | cut -d"_ES" -f 10-11 | cut -f1 -d "."`
	elif [[ ${AN_TEMP} == IN ]] || [[ ${AN_TEMP} == NV ]]
	then
		AN2=`echo $ZIP | cut -d"_ES" -f 1-2 | sed 's/InstalablesSH/''/'`
		if [[ ${AN2} == IN_IndComConsol ]]
		then
			AN=IICC
			Da=`echo $ZIP | cut -d"_ES" -f 4`
			VN=`echo $ZIP | cut -d"_ES" -f 5-9`
			T=`echo $ZIP | cut -d"_ES" -f 10-11 | cut -f1 -d "."`
		elif [[ ${AN2} == NV_VentasAdministrativasMstr ]]
		then
			AN=VVAA
			Da=`echo $ZIP | cut -d"_ES" -f 4`
			VN=`echo $ZIP | cut -d"_ES" -f 5-9`
			T=`echo $ZIP | cut -d"_ES" -f 10-11 | cut -f1 -d "."`
		fi
	elif [[ ${AN_TEMP} == RCM ]]
	then
		AN_TEMP2=`echo $ZIP | cut -d"_ES" -f 1-2 | sed 's/InstalablesSH/''/'`
		if [[ ${AN_TEMP2} == RCM_Extracciones ]]
		then
			AN=`echo ${AN_TEMP2}`
			Da=`echo $ZIP | cut -d"_ES" -f 4`
			VN=`echo $ZIP | cut -d"_ES" -f 5-9`
			T=`echo $ZIP | cut -d"_ES" -f 10-11 | cut -f1 -d "."`
		elif [[ ${AN_TEMP2} == RCM_ES ]]
		then
			AN=`echo ${AN_TEMP}`
			Da=`echo $ZIP | cut -d"_ES" -f 3`
			VN=`echo $ZIP | cut -d"_ES" -f 4-8`
			T=`echo $ZIP | cut -d"_ES" -f 9-10 | cut -f1 -d "."`
		fi
	elif [[ ${AN_TEMP} == Integrador ]]
	then
		AN=`echo ${AN_TEMP}RCM`
		Da=`echo $ZIP | cut -d"_ES" -f 3`
		VN=`echo $ZIP | cut -d"_ES" -f 4-8`
		T=`echo $ZIP | cut -d"_ES" -f 9-10 | cut -f1 -d "."`
	else
		AN=`echo ${AN_TEMP}`
		Da=`echo $ZIP | cut -d"_ES" -f 3`
		VN=`echo $ZIP | cut -d"_ES" -f 4-8`
		T=`echo $ZIP | cut -d"_ES" -f 9-10 | cut -f1 -d "."`
	fi
	EAR=`echo ${AN}_ES_${Da}_${VN}_${T}`.ear
	RAR=`echo ${AN}Configuration_ES_${Da}_${VN}_${T}`.rar
	mkdir /tmp/CRQ${CRQ_NUM}
	cd /tmp/CRQ${CRQ_NUM}
	if [[ -f /tmp/${ZIP} ]]
	then
		mv /tmp/${ZIP} /tmp/CRQ${CRQ_NUM}/${ZIP}
		unzip /tmp/CRQ${CRQ_NUM}/${ZIP} -d /tmp/CRQ${CRQ_NUM}/${IPN}
		cde=`~/tmp/CRQ${CRQ_NUM}/$IPN/install`
		cp $cde/execute_script.sh ~/tmp/CRQ${CRQ_NUM}
		cp $cde/post_install.sh ~/tmp/CRQ$CRQ_NUM
		cd ~/tmp/CRQ${CRQ_NUM}/$IPN/install
		chmod 755 $ZIP
		chmod 755 execute_script.sh
		chmod 755 post_install.sh
		
	else
		echo "[`date`]: Downloading package..."
		echo "[`date`]: Connecting to lep2cva1.es.wcorp.carrefour.com via ftp..."
		echo -e "Enter username: \c"
		read USER
		echo -e "Enter password: \c"
		read PASS
ftp -n lep2cva1.es.wcorp.carrefour.com <<END_SCRIPT
quote USER $USER
quote PASS $PASS
binary
cd $URL_PATH
get $ZIP
get $EAR
get $RAR
bye
END_SCRIPT
	fi
	if [[ -f /tmp/CRQ${CRQ_NUM}/${ZIP} ]]
	then
		unzip /tmp/CRQ${CRQ_NUM}/${ZIP} -d /tmp/CRQ${CRQ_NUM}/${IPN}
#****************************mark.p***********************************#
		cp /tmp/CRQ${CRQ_NUM}/${IPN}/install/pre_*.sh /tmp/CRQ${CRQ_NUM}
		cp /tmp/CRQ${CRQ_NUM}/${IPN}/install/execute_*.sh /tmp/CRQ${CRQ_NUM}
		cd /tmp/CRQ${CRQ_NUM}
		chmod 755 pre_*.sh
		chmod 755 execute_*.sh
		chmod 755 $ZIP
#****************************mark.p***********************************#
	else
		echo "[`date`]: Error - Package does not exist."
		exit 1
	fi
}
#++++++++++++++++++++++++++++++++++++++++++++Credits to the one who wrote the PACKAGE_DOWNLOAD script++++++++++++++++++++++++++++++++++++++++++++++#

PACKAGE_DOWNLOAD

#****************************mark.p***********************************#
DIR_CNG(){
clear
cd /tmp/CRQ${CRQ_NUM}/
}
	
PRESS_ENTER(){
    echo -n -e "\n\nPress \033[1mENTER\033[0m to Continue ..."
    read
    clear
}

PWDR(){
PWD=/tmp/CRQ${CRQ_NUM}
execute_script.sh cat_logs.sh $PWD
}

#Input details BX
#For Improvement:
#Import text files where all details are.
BBDD_UNIX(){
	clear
#	echo -e "App Name: \c"
#	read APP_NAM
	mkdir /tmp/CRQ${CRQ_NUM}/
	echo -e "Preinstall: \c"
	read PRE_INS
	echo -e "Database: \c"
	read BBD_DTB
	echo -e "Database U: \c"
	read BBD_DTBU
	echo -e "Unix Instalacion: \c"
	read EXS_DTB
	echo -e "Unix Uninstalacion: \c"
	read EXS_DTBU
	echo -e "Post Install: \c"
	read PST_INS
	
BBD_LOG=/tmp/CRQ${CRQ_NUM}/reinstall_BBDD.log
UNX_LOG=/tmp/CRQ$CRQ_NUM/reinstall_UNIX.log

#if [[ ${BBD_DTBN} ==  ]]
#then

	#DIR_CNG
	#echo "[                                                                                        ]"
	#$PRE_INS
	#echo "[///////////                                                                             ]"
	#$BBD_DTB
	#echo "[/////////////////////                                                                   ]"
	#PWDR
	#echo "[///////////////////////////////                                                         ]"
	#$EXS_DTB
	#echo "[/////////////////////////////////////////                                               ]"
	#PWDR
	#echo "[////////////////////////////////////////////////////                                    ]"
	#$PST_INS
	#echo "[//////////////////////////////////////////////////////////////                          ]"
	#PWDR
	#echo "[////////////////////////////////////////////////////////////////////////                ]"
	#echo "[////////////////////////////////////////////////////////////////////////////////////////]"
	
	DIR_CNG
	echo "[                                                                                         ]"
	$PRE_INS
	echo "[///                                                                                      ]"
	$BBD_DTB
	PWDR
	echo "[///////                                                                                  ]"
	$EXS_DTB
	echo "[///////////                                                                              ]"
	PWDR
	$PST_INS
	PWDR
	echo "[/////////////////////                                                                   ]"
	$PRE_INS
#if [[ ${BBD_DTBU} ==  ]]
#then
	$BBD_DTBU
	PWDR
	echo "[///////////////////////////////                                                         ]"
#else
#	echo "[///////////////////////////////                                                         ]"
#fi
#if [[ ${EXS_DTBU} ==  ]]
	$EXS_DTBU
	PWDR
	echo "[/////////////////////////////////////////                                               ]"
	$PST_INS
	PWDR
	echo "[////////////////////////////////////////////////////                                    ]"
	$PRE_INS
	$BBD_DTB >> $BBD_LOG
	echo "[//////////////////////////////////////////////////////////////                          ]"
	$EXS_DTB >> $UNX_LOG
	echo "[////////////////////////////////////////////////////////////////////////                ]"
	$PST_INS
	PWDR
	echo "[////////////////////////////////////////////////////////////////////////////////////////]"
	ls
	cd ~/versiones
	ls -ltr *${AN_TEMP}*
	echo 'Thank you for using ssync!!! Go to Filezilla and get your logs.'
			#PRESS_ENTER;;
}

#Input details for X
UNIX_ONLY(){
	clear
#	echo -e "App Name: \c"
#	read APP_NAM
	mkdir /tmp/CRQ${CRQ_NUM}/
	echo -e "Preinstall: \c"
	read PRE_INS
#	echo -e "Database: \c"
#	read BBD_DTB
#	echo -e "Database U: \c"
#	read BBD_DTBU
	echo -e "Unix Instalacion: \c"
	read EXS_DTB
	echo -e "Unix Uninstalacion: \c"
	read EXS_DTBU
	echo -e "Post Install: \c"
	read PST_INS
	
#BBD_LOG=/tmp/CRQ${CRQ_NUM}/reinstall_BBDD.log
UNX_LOG=/tmp/CRQ$CRQ_NUM/reinstall_UNIX.log
#UNIXU= `echo $EXS_DTB | sed 's/install/uninstall/g'`

#if [[ ${BBD_DTBN} ==  ]]
#then

	#DIR_CNG
	#echo "[                                                                                        ]"
	#$PRE_INS
	#echo "[///////////                                                                             ]"
	#$BBD_DTB
	#echo "[/////////////////////                                                                   ]"
	#PWDR
	#echo "[///////////////////////////////                                                         ]"
	#$EXS_DTB
	#echo "[/////////////////////////////////////////                                               ]"
	#PWDR
	#echo "[////////////////////////////////////////////////////                                    ]"
	#$PST_INS
	#echo "[//////////////////////////////////////////////////////////////                          ]"
	#PWDR
	#echo "[////////////////////////////////////////////////////////////////////////                ]"
	#echo "[////////////////////////////////////////////////////////////////////////////////////////]"
	
	DIR_CNG
	echo "[                                                                                         ]"
	$PRE_INS
	echo "[///                                                                                      ]"
	$BBD_DTB
	PWDR
	echo "[///////                                                                                  ]"
	$EXS_DTB
	echo "[///////////                                                                              ]"
	PWDR
	$PST_INS
	PWDR
	echo "[/////////////////////                                                                   ]"
	$PRE_INS
#if [[ ${BBD_DTBU} ==  ]]
#then
#	$BBD_DTBU
#	PWDR
	echo "[///////////////////////////////                                                         ]"
#else
#	echo "[///////////////////////////////                                                         ]"
#fi
#if [[ ${EXS_DTBU} ==  ]]
	$EXS_DTBU
#	$UNIXU
	PWDR
	echo "[/////////////////////////////////////////                                               ]"
	$PST_INS
	PWDR
	echo "[////////////////////////////////////////////////////                                    ]"
	$PRE_INS
#	$BBD_DTB >> $BBD_LOG
	echo "[//////////////////////////////////////////////////////////////                          ]"
	$EXS_DTB >> $UNX_LOG
	echo "[////////////////////////////////////////////////////////////////////////                ]"
	$PST_INS
	PWDR
	echo "[////////////////////////////////////////////////////////////////////////////////////////]"
	ls
	cd ~/versiones
	ls -ltr *${AN_TEMP}*
	echo 'Thank you for using ssync!!! Go to Filezilla and get your logs.'
			#PRESS_ENTER;;
}

#cd /tmp/CRQ${CRQ_NUM}/$IPN/install
#mv pre_install.sh /tmp/CRQ$CRQ_NUM
#mv execute_script.sh /tmp/CRQ$CRQ_NUM
#chmod 755 /tmp/CRQ$CRQ_NUM/pre_install.sh
#chmod 755 /tmp/CRQ$CRQ_NUM/execute_script.sh
	
	
	
#Choices
until [ "$UserType" = "0" ];
do
clear
echo -e "\e[1;33mChoose which Component(s) you need to install\e[0m \n"
echo -e "[\e[1;33m1\e[0m] BBDD UNIX"
echo -e "[\e[1;33m2\e[0m] UNIX"
echo -e "[\e[1;33m0\e[0m] Exit"
echo -e "\033[1mPlease Select Action Item:\033[0m \c"
read UserType

case $UserType in	
		1)
		BBDD_UNIX
		ls
		cd ~/versiones
		ls -ltr *${AN_TEMP}*
		echo 'Thank you for using ssync!!! Go to Filezilla and get your logs.'
		PRESS_ENTER;;
		2)
		UNIX_ONLY
		ls
		cd ~/versiones
		ls -ltr *${AN_TEMP}*
		echo 'Thank you for using ssync!!! Go to Filezilla and get your logs.'
		PRESS_ENTER;;
	esac
done

#Alternative:
#Download package to the machine
#Copy the files required by the installation to their designated directory
#Remove the downloaded/extracted package
#Then this script.
#String example:
#execute_script.sh apliccoper Dofoplus post_install.sh
#$1 = apliccoper $2 Dofoplus
#
#
#
#
#
#
#
#
#
#


